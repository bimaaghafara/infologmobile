﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using InfologMobile.Rest.Service;
using InfologMobile.ApplicationProperties.Service;
using InfologMobile.UserAuth.Dto;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace InfologMobile.Auth.Service
{
    public class AuthService
    {
        RestService restService = new RestService();
        ApplicationPropertiesService applicationPropertiesService = new ApplicationPropertiesService();

        public async Task<AuthResultDto> Login(AuthDto userAuth)
        {
            AuthResultDto result = await restService.Post<AuthResultDto, AuthDto>(userAuth, "/Auth");

            // save auth to Application.Current.Properties (localStorage)
            applicationPropertiesService.SaveAuthToApplicationProperties(result);

            return result;
        }

        public void Logout(object sender)
        {
            Application.Current.Properties.Clear();
            MessagingCenter.Send(sender, App.EVENT_LAUNCH_LOGIN_PAGE);
        }

    }
}