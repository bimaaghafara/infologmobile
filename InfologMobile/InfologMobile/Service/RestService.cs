﻿//using Newtonsoft.Json;
using InfologMobile.ApplicationProperties.Service;
using InfologMobile.UserAuth.Dto;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;

namespace InfologMobile.Rest.Service
{
    public class RestService
    {
        public HttpClient httpClient;
        ApplicationPropertiesService applicationPropertiesService = new ApplicationPropertiesService();

        public RestService()
        {
            httpClient = new HttpClient
            {
                MaxResponseContentBufferSize = 256000
            };
        }

        public void AddHttpHeaders()
        {
            if (Application.Current.Properties.ContainsKey("Auth"))
            {
                AuthResultDto Auth = applicationPropertiesService.GetAuthFromApplicationProperties();
                if (httpClient.DefaultRequestHeaders.Contains("Token"))
                {
                    httpClient.DefaultRequestHeaders.Remove("Token");
                }
                httpClient.DefaultRequestHeaders.Add("Token", Auth.Token);
            }
        }

        public void RefreshToken(HttpResponseMessage response)
        {
            try
            {
                // newToken from responseHeaders
                string newToken = response.Headers.GetValues("Token").FirstOrDefault();

                // GetAuthFromApplicationProperties and update to newToken
                AuthResultDto Auth = applicationPropertiesService.GetAuthFromApplicationProperties();
                Auth.Token = newToken;

                // save auth (with new token) to Application.Current.Properties (localStorage)
                applicationPropertiesService.SaveAuthToApplicationProperties(Auth);
            } catch (Exception ex)
            {
                throw new Exception("error from responseHeaders");
            }
        }

        public async Task<TResponse> Post<TResponse, TPayload>(TPayload Payload, string subUrl)
        {
            // initial content for httpClient.PostAsync
            string url = AppConstant.WebApi + subUrl;
            Uri uri = new Uri(string.Format(url, string.Empty));
            string jsonStringPayload = JsonConvert.SerializeObject(Payload);
            StringContent content = new StringContent(jsonStringPayload, Encoding.UTF8, "application/json");

            // add auth.token from localStorage(Application.Current.Properties) to request headers
            if (subUrl.ToLower() != "/auth") AddHttpHeaders();

            HttpResponseMessage response = await httpClient.PostAsync(uri, content);
            string jsonStringResponse = await response.Content.ReadAsStringAsync();

            // RefreshToken from response headers
            if (subUrl.ToLower() != "/auth") RefreshToken(response);

            // DeserializeObject response from httpClient.PostAsync
            TResponse result = JsonConvert.DeserializeObject<TResponse>(jsonStringResponse);
            return result;
        }
    }
}
