﻿using InfologMobile.UserAuth.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace InfologMobile.ApplicationProperties.Service
{
    class ApplicationPropertiesService
    {
        public void SaveToApplicationProperties<TValue>(TValue Value, string Key)
        {
            Application.Current.Properties[Key] = JsonConvert.SerializeObject(Value);
        }

        //public TValue GetFromApplicationProperties<TValue>(string Key)
        //{
        //    TValue result;
        //    if (Application.Current.Properties.ContainsKey(Key))
        //    {
        //        string AuthString = Application.Current.Properties[Key].ToString();
        //        result = JsonConvert.DeserializeObject<TValue>(AuthString);
        //        return result;
        //    }
        //    return new object;
        //}

        public void SaveAuthToApplicationProperties(AuthResultDto Value)
        {
            Application.Current.Properties["Auth"] = JsonConvert.SerializeObject(Value);
        }

        public AuthResultDto GetAuthFromApplicationProperties()
        {
            if (Application.Current.Properties.ContainsKey("Auth"))
            {
                string AuthString = Application.Current.Properties["Auth"].ToString();
                AuthResultDto result = JsonConvert.DeserializeObject<AuthResultDto>(AuthString);
                return result;
            }
            return new AuthResultDto();
        }
    }
}
