﻿using System.Collections.Generic;

namespace InfologMobile.UserAuth.Dto
{
    public class AuthDto
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public AuthDto(string userId, string password)
        {
            UserId = userId;
            Password = password;
        }
    }

    public class AuthResultDto
    {
        public bool IsAuthorized { get; set; }
        public string Token { get; set; }
        public UserAuthDto User { get; set; }
    }

    public class UserAuthDto
    {
        public IEnumerable<AuthCompanyDto> Companies { get; set; }

        public string DateFormat { get; set; }

        public int DefaultCompanyId { get; set; }

        public int DefaultOwnerId { get; set; }

        public string DefaultOwnerLabel { get; set; }

        public int DefaultWarehouseId { get; set; }

        public string DefaultWarehouseLabel { get; set; }

        public string Email { get; set; }

        public bool IsAdministrator { get; set; }

        public string Locale { get; set; }

        public string LoginName { get; set; }

        public string TimeZone { get; set; }
    }

    public class AuthCompanyDto
    {
        public int CompanyId { get; set; }
    }
}
