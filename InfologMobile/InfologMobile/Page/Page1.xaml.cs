﻿using InfologMobile.Auth.Service;
using InfologMobile.Rest.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InfologMobile.Page
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{

        RestService restService = new RestService();
        AuthService authService = new AuthService();

        public Page1 ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
        }

        async void TesOperateColumn(object sender, EventArgs eventArgs)
        {
            OperateColumnPayloadDto tes = new OperateColumnPayloadDto(new string[0], 1, 10, 1);
            OperateColumnResultDto result = await restService.Post<OperateColumnResultDto, OperateColumnPayloadDto>(tes, "/Owner/OperateColumn");
        }

        void Logout(object sender, EventArgs e)
        {
            authService.Logout(this);
        }

    }

    public class OperateColumnResultDto
    {
        public List<object> Data { get; set; }
        public int TotalRecord { get; set; }
    }

    public class OperateColumnPayloadDto
    {
        public string[] Filters { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int SortDirection { get; set; }


        public OperateColumnPayloadDto(string[] filters, int pageNumber, int pageSize, int sortDirection)
        {
            Filters = filters;
            PageNumber = pageNumber;
            PageSize = pageSize;
            SortDirection = sortDirection;
        }
    }
}