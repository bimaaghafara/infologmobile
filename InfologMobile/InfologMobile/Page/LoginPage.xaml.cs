﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using InfologMobile.UserAuth.Dto;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Net.Http;

using InfologMobile.Auth.Service;

namespace InfologMobile.Page
{
    public partial class LoginPage : ContentPage
    {
        protected string newButtonText = "new button!";
        protected string WebApi = "http://rnd.infolog.com.sg:81/WebAPI/api/";
        AuthService authService = new AuthService();

        //Constructor
        public LoginPage()
        {
            InitializeComponent();
 
            // for testing only
            entryUserId.Text = "test2";
            entryPassword.Text = "password";
        }

        protected override void OnAppearing()
        {
            //custom component
            messageLabel.Text = null;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }


        async void OnLoginAsync(object sender, EventArgs eventArgs)
        {
            AuthDto userAuth = new AuthDto(entryUserId.Text, entryPassword.Text);
            buttonLogin.IsEnabled = false;
            messageLabel.Text = "Loading . . .";
            AuthResultDto result = await authService.Login(userAuth);
            buttonLogin.IsEnabled = true;
            try
            {
                if (result.IsAuthorized)
                {
                    messageLabel.Text = "Success login!";

                    Navigation.InsertPageBefore(new Page1(), this);
                    await Navigation.PopAsync();

                    //just for example
                    //await Navigation.PushAsync(new Page1());
                } else
                {
                    messageLabel.Text = "Fail login!";
                }
            }
            catch (Exception exception)
            {
                messageLabel.Text = "Fail login!";
            }
        }

        void OnForgot(object sender, EventArgs eventArgs)
        {
            Debug.WriteLine("OnForgot!");
            if (Application.Current.Properties.ContainsKey("Auth"))
            {
                AuthResultDto Auth = JsonConvert.DeserializeObject<AuthResultDto>(Application.Current.Properties["Auth"].ToString());
                Debug.WriteLine("");
            }
        }
    }
}
