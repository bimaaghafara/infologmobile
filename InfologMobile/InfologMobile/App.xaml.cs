﻿using System;
using InfologMobile.ApplicationProperties.Service;
using InfologMobile.Page;
using InfologMobile.UserAuth.Dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace InfologMobile
{
    public partial class App : Application
    {
        public static string EVENT_LAUNCH_LOGIN_PAGE = "EVENT_LAUNCH_LOGIN_PAGE";
        ApplicationPropertiesService applicationPropertiesService = new ApplicationPropertiesService();

        public App()
        {
            // Initialize Live Reload.
            #if DEBUG
            LiveReload.Init();
            #endif

            InitializeComponent();
            AuthResultDto Auth = applicationPropertiesService.GetAuthFromApplicationProperties();
            if (!Auth.IsAuthorized)
            {
                MainPage = new NavigationPage(new LoginPage());
            } else
            {
                MainPage = new NavigationPage(new Page1());
            }

            MessagingCenter.Subscribe<object>(this, EVENT_LAUNCH_LOGIN_PAGE, SetLoginPageAsRootPage);
        }

        private void SetLoginPageAsRootPage(object sender)
        {
            MainPage = new NavigationPage(new LoginPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
